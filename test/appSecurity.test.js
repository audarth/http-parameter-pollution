const request = require('supertest');
const app = require('./app');


describe('security', () => {

    it('Request to transfer and withdraw $100, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer%3Faction=withdraw', amount: '100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('illegal action');
    });

    it('Request to transfer and withdraw -$100, should return 400 with error message', async () => {
        const res = await request(app)
            .get('/')
            .query({action: 'transfer', amount: '-100'})
        expect(res.statusCode).toEqual(400);
        expect(res.text).toContain('amount not in valid range');
    });
    
});
