"use strict";

// requirements
const express = require("express");
const payment = require("./payment");

// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get("/status", (req, res) => {
  res.status(200).end();
});
app.head("/status", (req, res) => {
  res.status(200).end();
});

const actionsAllowed = ["transfer", "withdraw"];

app.get("/", (req, res) => {
  // We only allow transfer
  if (!actionsAllowed.includes(req.query.action)) {
    res.status(400).send("illegal action");
  }

  if (req.query.amount === undefined || typeof req.query.amount !== "string") {
    return res.status(400).send("You must specify the amount");
  }

  const MIN = 0;
  const MAX = 10000000;
  const MAX_LENGTH = 8;

  if (req.query.amount.length > MAX_LENGTH) {
    return res.status(400).send(`length above ${MAX_LENGTH}`);
  }

  const amount = parseInt(req.query.amount, 10);

  if (amount <= MIN || amount > MAX) {
    return res.status(400).send("amount not in valid range");
  }

  if (req.query.action === "transfer") {
    res.send(payment(req.query.action, amount));
    return;
  }
  res.status(400).send("You can only transfer an amount");
});

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
  // HTTP listener
  app.listen(PORT, (err) => {
    if (err) {
      console.log(err);
      process.exit(1);
    }
    console.log("Server is listening on port: ".concat(PORT));
  });
}
// CTRL+c to come to action
process.on("SIGINT", function () {
  process.exit();
});

module.exports = app;
