function payment(action, amount) {
  if (amount === undefined) {
    return "You must specify the amount";
  }

  if (action === "withdraw") {
    return `Successfuly withdrawed $${amount}`;
  } else if (action === "transfer") {
    return `Successfully transfered $${amount}`;
  }
  return "You must specify an action: withdraw or transfer";
}

module.exports = payment;
